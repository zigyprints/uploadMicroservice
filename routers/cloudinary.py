from fastapi import APIRouter, UploadFile, File, Query
from controllers import cloudinary

router = APIRouter()

@router.post("/cloudinary/upload/")
async def upload_image(folder_name: str, file: UploadFile = File(...)):
    return await cloudinary.upload_file(file, folder_name)


@router.get("/cloudinary/files/")
def get_files(folder_path: str = Query(None), lower_limit: int = Query(0), upper_limit: int = Query(10)):
    """
    Retrieves a specified number of files from a virtual 'folder' on GCS.

    Args:
        folder_path (str, optional): Path to the virtual 'folder' (without trailing slash). Defaults to None.
        lower_limit (int, optional): Lower limit (inclusive) of the number of files to retrieve. Defaults to 0.
        upper_limit (int, optional): Upper limit (exclusive) of the number of files to retrieve. Defaults to 10.

    Returns:
        dict: A dictionary containing the retrieved files and their total count.
    """

    files = cloudinary.get_files_from_folder(folder_path, lower_limit, upper_limit)  # Remove await here
    num_files = len(files)
    return {
        "files": files,
        "total_files": num_files,
    }
