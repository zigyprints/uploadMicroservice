from fastapi import APIRouter, UploadFile, File, Query
from controllers import aws as s3  # Import the S3 controller

router = APIRouter()


@router.post("/aws-s3/upload/")
async def upload_image(folder_name: str, file: UploadFile = File(...)):
    return await s3.upload_image(file, folder_name)


@router.get("/aws-s3/files/")
def get_files(folder_path: str = Query(None), lower_limit: int = Query(0), upper_limit: int = Query(10)):
    files = s3.get_files_from_folder(folder_path, lower_limit, upper_limit)
    num_files = len(files)
    return {"files": files, "total_files": num_files}
