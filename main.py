from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import gcs, cloudinary, aws

app = FastAPI()

# Allow CORS from specific origins (replace with your desired origins)
allowed_origins = ["http://localhost:3000", "http://35.200.207.184","http://localhost:5000"]

# Optional: Allow all origins for development purposes (not recommended for production)
# allowed_origins = ["*"]

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=allowed_origins,
    allow_credentials=True,  # Allow cookies and other credential info
    allow_methods=["*"],  # Allow all HTTP methods (GET, POST, PUT, DELETE, etc.)
    allow_headers=["*"],  # Allow all headers (Content-Type, Authorization, etc.)
)

app.include_router(gcs.router)
app.include_router(cloudinary.router)
app.include_router(aws.router)
