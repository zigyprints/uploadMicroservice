import time
from fastapi import UploadFile
import boto3
import hashlib
import os


def create_s3_client():
    """Creates an S3 client using credentials from environment variables."""
    return boto3.client(
        "s3",
        aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID"),
        aws_secret_access_key=os.environ.get("AWS_SECRET_ACCESS_KEY"),
    )


async def upload_image(file: UploadFile, folder_name: str):
    """Uploads an image to S3 and returns its filename and URL."""
    s3_client = create_s3_client()
    bucket_name = os.environ.get("AWS_S3_BUCKET_NAME")
    if not bucket_name:
        raise ValueError("Missing environment variable AWS_S3_BUCKET_NAME")

    timestamp_hash = hashlib.sha256(str(time.time()).encode()).hexdigest()[:10]
    filename = f"{folder_name}/{timestamp_hash}-{file.filename}"

    s3_client.upload_fileobj(
        file.file, bucket_name, filename, ExtraArgs={"ContentType": file.content_type}
    )

    object_url = f"https://{bucket_name}.s3.amazonaws.com/{filename}"
    return {"filename": filename, "url": object_url}


def get_files_from_folder(folder_path: str, lower_limit: int, upper_limit: int):
    """Gets a specified number of files from a virtual 'folder' on S3."""
    s3_client = create_s3_client()
    bucket_name = os.environ.get("AWS_S3_BUCKET_NAME")
    if not bucket_name:
        raise RuntimeError("Missing environment variable AWS_S3_BUCKET_NAME")

    prefix = f"{folder_path}/" if folder_path else ""
    paginator = s3_client.get_paginator("list_objects_v2")
    page_iterator = paginator.paginate(Bucket=bucket_name, Prefix=prefix)

    filtered_files = []
    for page in page_iterator:
        if "Contents" in page:
            for obj in page["Contents"]:
                if lower_limit <= len(filtered_files) < upper_limit:
                    filtered_files.append(
                        {
                            "name": obj["Key"],
                            "url": f"https://{bucket_name}.s3.amazonaws.com/{obj['Key']}",
                        }
                    )

    return filtered_files
