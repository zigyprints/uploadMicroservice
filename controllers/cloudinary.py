import time
from fastapi import UploadFile,HTTPException
import cloudinary
from cloudinary.uploader import upload
import hashlib
import os

async def upload_file(file: UploadFile, folder_name: str):
    cloudinary.config(
        cloud_name=os.environ.get("CLOUDINARY_CLOUD_NAME"),
        api_key=os.environ.get("CLOUDINARY_API_KEY"),
        api_secret=os.environ.get("CLOUDINARY_API_SECRET"),
    )

    # Generate unique filename based on timestamp and original filename
    timestamp_hash = hashlib.sha256(str(time.time()).encode()).hexdigest()[:10]
    filename = f"{folder_name}/{timestamp_hash}-{file.filename}"
    
    print("File",file)

    try:
        # Upload the file to Cloudinary
        result = upload(file.file, public_id=filename, resource_type="image")

        # print("Data", file, result)

        return {"filename": filename, "url": result.get("secure_url")}  # Use secure_url

    except Exception as e:
        raise RuntimeError(f"Cloudinary upload failed: {e}") from e  # Clear error message




from cloudinary.api import resources

def get_files_from_folder(folder_path: str, lower_limit: int, upper_limit: int):
    try:
        # Fetch a list of images in the specified folder within the specified range
        images = resources(
            type="upload",
            prefix=folder_path,
            max_results=100,  # Adjust max_results as needed
        )
        return images["resources"][lower_limit:upper_limit]
    except cloudinary.exceptions.Error as e:
        raise HTTPException(
            status_code=500, detail="Failed to retrieve images"
        )
