import time
from fastapi import UploadFile
from google.cloud import storage
import hashlib
import os

async def upload_image(file: UploadFile, folder_name: str):
    storage_client = storage.Client()
    bucket_name = os.environ.get('GCS_BUCKET_NAME')
    if not bucket_name:
        raise ValueError("Missing environment variable BUCKET_NAME")

    bucket = storage_client.bucket(bucket_name)


    # Generate unique filename based on timestamp and original filename
    timestamp_hash = hashlib.sha256(str(time.time()).encode()).hexdigest()[:10]
    filename = f"{folder_name}/{timestamp_hash}-{file.filename}"

    blob = bucket.blob(filename)

    # Upload the file to GCS
    blob.upload_from_string(
        file.file.read(),
        content_type=file.content_type
    )

    # Make the blob public (optional, consider alternatives)
    blob.make_public()

    print("Data", file, blob)

    return {"filename": filename, "url": blob.public_url}



def get_files_from_folder(folder_path: str, lower_limit: int, upper_limit: int):
    """
    Retrieves a specified number of files from a virtual 'folder' on GCS, including file names and URLs.

    Args:
        folder_path (str): Path to the virtual 'folder' (without trailing slash).
        lower_limit (int): Lower limit (inclusive) of the number of files to retrieve.
        upper_limit (int): Upper limit (exclusive) of the number of files to retrieve.

    Returns:
        list: List of dictionaries containing file information.

    Raises:
        ValueError: If lower limit is greater than upper limit.
        RuntimeError: If the GCS_BUCKET_NAME environment variable is not set.
    """

    if lower_limit > upper_limit:
        raise ValueError("Lower limit cannot be greater than upper limit")

    try:
        storage_client = storage.Client()
        bucket_name = os.environ.get('GCS_BUCKET_NAME')
        if not bucket_name:
            raise RuntimeError("Missing environment variable GCS_BUCKET_NAME")

        bucket = storage_client.bucket(bucket_name)

        # Construct the prefix for the folder path
        prefix = f"{folder_path}/" if folder_path else ""

        # Use list_blobs with pagination and filtering
        blobs = bucket.list_blobs(prefix=prefix)

        # Initialize empty list for filtered blobs
        filtered_blobs = []

        for blob in blobs:
            if lower_limit <= len(filtered_blobs) < upper_limit:
                filtered_blobs.append(blob)

        # Extract information from the filtered blobs and construct URLs
        data = [
            {
                "name": blob.name,
                "url": f"https://storage.googleapis.com/{bucket_name}/{blob.name}"
            }
            for blob in filtered_blobs
        ]
        return data

    except Exception as e:
        raise e  # Re-raise the exception for better error handling
